Support Vector Machine and k-Nearest Neighbor Algorithm. Tasks:

1 Support Vector Machine Classifier (SVM)

2 Nonlinear SVM

3 SVM hyperparameters optimization

4 Influence of feature standardization in SVM

5 Algorithm of k-nearest neighbors

6 Analysis of the k-nearest neighbor algorithm

7 "Curse of Dimensionality"

Implemented in Python using NumPy, scikit-learn, pandas and Matplotlib library. Code and task description in the Jupyter Notebook.

My lab assignment in Machine Learning, FER, Zagreb.

Created: 2020
